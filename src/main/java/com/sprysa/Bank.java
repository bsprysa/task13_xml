package com.sprysa;

import java.math.BigDecimal;

public class Bank {
  private int bankID;
  private String name;
  private String type;
  private String depositor;
  private long account_id;
  private BigDecimal amount_on_deposit;
  private double profitability;
  private int time_constraints;

  public Bank() {
  }

  public Bank(int bankID, String name, String type, String depositor, long account_id,
      BigDecimal amount_on_deposit, double profitability, int time_constraints) {
    this.bankID = bankID;
    this.name = name;
    this.type = type;
    this.depositor = depositor;
    this.account_id = account_id;
    this.amount_on_deposit = amount_on_deposit;
    this.profitability = profitability;
    this.time_constraints = time_constraints;
  }

  public int getBankID() {
    return bankID;
  }

  public void setBankID(int bankID) {
    this.bankID = bankID;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getDepositor() {
    return depositor;
  }

  public void setDepositor(String depositor) {
    this.depositor = depositor;
  }

  public long getAccount_id() {
    return account_id;
  }

  public void setAccount_id(long account_id) {
    this.account_id = account_id;
  }

  public BigDecimal getAmount_on_deposit() {
    return amount_on_deposit;
  }

  public void setAmount_on_deposit(BigDecimal amount_on_deposit) {
    this.amount_on_deposit = amount_on_deposit;
  }

  public double getProfitability() {
    return profitability;
  }

  public void setProfitability(double profitability) {
    this.profitability = profitability;
  }

  public int getTime_constraints() {
    return time_constraints;
  }

  public void setTime_constraints(int time_constraints) {
    this.time_constraints = time_constraints;
  }

  @Override
  public String toString() {
    return "Bank{" +
        "bankID=" + bankID +
        ", name='" + name + '\'' +
        ", type='" + type + '\'' +
        ", depositor='" + depositor + '\'' +
        ", account_id=" + account_id +
        ", amount_on_deposit=" + amount_on_deposit +
        ", profitability=" + profitability +
        ", time_constraints=" + time_constraints +
        '}';
  }
}
