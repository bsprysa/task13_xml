package com.sprysa;

public enum DepositType {
  BY_DEMAND, TERM, PAYMENT, ACCUMULATIVE, OCHAD, METALS
}
