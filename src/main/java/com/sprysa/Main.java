package com.sprysa;

import java.io.File;
import java.util.List;

public class Main {

  public static void main(String[] args) {
    File xml = new File("src\\main\\resources\\Banks.xml");
    printList(StAX.parseBanks(xml), "StAX");
  }
  private static void printList(List<Bank> banks, String parserName) {
    System.out.println(parserName);
    for (Bank bank : banks) {
      System.out.println(bank);
    }
  }

}
