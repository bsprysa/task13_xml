package com.sprysa;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

public class StAX {

  public static List<Bank> parseBanks(File xml) {
    List<Bank> bankList = new ArrayList<Bank>();
    Bank bank = null;

    XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
    XMLEvent xmlEvent;
    try {
      XMLEventReader xmlEventReader = xmlInputFactory
          .createXMLEventReader(new FileInputStream(xml));
      while (xmlEventReader.hasNext()) {
        xmlEvent = xmlEventReader.nextEvent();
        if (xmlEvent.isStartElement()) {
          StartElement startElement = xmlEvent.asStartElement();
          String name = startElement.getName().getLocalPart();

          if (name == "bank") {
            bank = new Bank();
            Attribute idAttr = startElement.getAttributeByName(new QName("bankID"));
            if (idAttr != null) {
              bank.setBankID(Integer.parseInt(idAttr.getValue()));
            }
          } else if (name == "Type") {
            xmlEvent = xmlEventReader.nextEvent();
            assert bank != null;
            bank.setType(xmlEvent.asCharacters().getData());
          }
        }
      }

      if (xmlEvent.isEndElement()) {
        EndElement endElement = xmlEvent.asEndElement();
        if (endElement.getName().getLocalPart().equals("bank")) {
          bankList.add(bank);
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return bankList;
  }
}
